﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TINA.Repositories.DataContextStorage
{
    public interface IDataContextStorageContainer<T>
    {
        T GetDataContext();
        void Store(T context);
        void Clear();
    }
}
