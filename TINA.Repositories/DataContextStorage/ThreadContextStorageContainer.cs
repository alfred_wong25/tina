﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;

namespace TINA.Repositories.DataContextStorage
{
    public class ThreadContextStorageContainer<T> : IDataContextStorageContainer<T> where T : class
    {
        private static readonly Hashtable __storedContexts = new Hashtable();
        public void Clear()
        {
            if (__storedContexts.Contains(GetThreadName()))
                __storedContexts[GetThreadName()] = null;
        }

        public T GetDataContext()
        {
            if (__storedContexts.Contains(GetThreadName()))
                return (T)__storedContexts[GetThreadName()];
            else
                return null;
        }

        public void Store(T context)
        {
            if (__storedContexts.Contains(GetThreadName()))
                __storedContexts[GetThreadName()] = context;
            else
                __storedContexts.Add(GetThreadName(), context);
        }

        private static string GetThreadName()
        {
            if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
                Thread.CurrentThread.Name = Guid.NewGuid().ToString();
            return Thread.CurrentThread.Name;
        }
    }
}
