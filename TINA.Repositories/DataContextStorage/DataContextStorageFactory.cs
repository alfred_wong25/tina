﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TINA.Repositories.DataContextStorage
{
    public class DataContextStorageFactory<T> where T : class
    {
        private static IDataContextStorageContainer<T> __dataStorageContainer;

        public static IDataContextStorageContainer<T> CreateDataStorageContainer()
        {
            if (__dataStorageContainer != null)
            {
                if (HttpContext.Current != null)
                    __dataStorageContainer = new HttpContextStorageContainer<T>();
                else
                    __dataStorageContainer = new ThreadContextStorageContainer<T>();
            }
            return __dataStorageContainer;
        }
    }
}
