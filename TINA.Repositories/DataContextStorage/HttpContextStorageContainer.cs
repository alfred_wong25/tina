﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TINA.Repositories.DataContextStorage
{
    public class HttpContextStorageContainer<T> : IDataContextStorageContainer<T> where T : class
    {
        private const string __dataContextKey = "TINAContext";
        public void Clear()
        {
            if (HttpContext.Current.Items.Contains(__dataContextKey))
                HttpContext.Current.Items[__dataContextKey] = null;
            //need to check if we need to actually disposed the item though?
        }

        public T GetDataContext()
        {
            if (HttpContext.Current.Items.Contains(__dataContextKey))
                return (T)HttpContext.Current.Items[__dataContextKey];
            else
                return null;
        }

        public void Store(T context)
        {
            if (HttpContext.Current.Items.Contains(__dataContextKey))
                HttpContext.Current.Items[__dataContextKey] = context;
            else
                HttpContext.Current.Items.Add(__dataContextKey, context);
        }
    }
}
