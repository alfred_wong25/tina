﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TINA.Repositories
{
    public interface IRepository<T, K> where T : class
    {
        T FindById(K id);

        /// <summary>
        /// Adds a new entity into repository.
        /// </summary>
        /// <param name="entity">Entity to add.</param>
        /// <returns>The ID of the entity added, if successful</returns>
        K Add(T entity);


        void Save();
        void Remove();

    }
}
