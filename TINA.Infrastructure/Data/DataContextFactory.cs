﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TINA.Repositories.DataContextStorage;

namespace TINA.Infrastructure.Data
{
    public static class DataContextFactory
    {
        public static void Clear()
        {
            var container = DataContextStorageFactory<TinaManagerContext>.CreateDataStorageContainer();
            container.Clear();
        }
        public static TinaManagerContext GetDataContext()
        {
            var container = DataContextStorageFactory<TinaManagerContext>.CreateDataStorageContainer();
            var context = container.GetDataContext();
            if (context == null)
            {
                context = new TinaManagerContext();
                container.Store(context);
            }
            return context;
        }
    }
}
